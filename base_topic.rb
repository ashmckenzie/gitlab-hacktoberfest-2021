# frozen_string_literal: true

require 'net/http'

PRODUCT_FILE_URL = 'https://gitlab.com/gitlab-data/analytics/-/raw/master/transform/snowflake-dbt/data/projects_part_of_product.csv'
NEW_TOPIC_TO_ADD = 'hacktoberfest'

PROJECT_NAMES_TO_IGNORE = Regexp.union(
  %r{\Agitlab-org/security/},
  %r{\Agitlab-org/gitlab-foss\z}
)

def projects_has_topic?(project, topic)
  project['topics'].include?(topic)
end

def put_topics_for_project(id, topics)
  topics_comma_separated = topics.join(', ')

  gitlab_put("/projects/#{id}", topics: topics_comma_separated)
end

def update_topics_for_project(project, topics)
  message_line_base = project_detail_line_for(project)
  json_response = put_topics_for_project(project['id'], topics)

  if json_response.data.key?('id')
    true
  else
    error("could not update - #{message_line_base}, error=[#{json_response.data['message']}]")
    false
  end
end

def project_should_be_ignored?(project_name)
  PROJECT_NAMES_TO_IGNORE.match?(project_name)
end

def project_is_public?(project)
  project['visibility'] == 'public'
end

def product_file_rows
  Net::HTTP.get(URI.parse(PRODUCT_FILE_URL)).split("\n")[1..-1]
end

def each_project_from_csv
  product_file_rows.each do |line|
    project_name, project_id = line.chomp.split(',')
    message_line_base = "project_name=[#{project_name}], project_id=[#{project_id}]"

    next if !project_name || !project_id

    debug("line: #{message_line_base}")

    response = get_project(project_id)
    unless response.code == '200'
      error("unable to find project - #{message_line_base}")
      next
    end

    if project_should_be_ignored?(project_name)
      info("skipped, ignored project - #{message_line_base}")
      next
    end

    project = response.data

    unless project_is_public?(project)
      info("skipped, non-public project - #{message_line_base}")
      next
    end

    yield project
  end
end

def project_detail_line_for(project)
  "name=[#{project['name']}], web_url=[#{project['web_url']}]"
end
