#!/usr/bin/env /Users/ash/.asdf/shims/ruby

# frozen_string_literal: true

require_relative 'base'
require_relative 'base_topic'

$stdout.sync = true

def remove_topic_from_project(project)
  new_topics = project['topics'].clone
  new_topics.delete(NEW_TOPIC_TO_ADD)
  debug("project: #{project_detail_line_for(project)}, current_topics=#{project['topics']}, new_topics=#{new_topics}")

  update_topics_for_project(project, new_topics)
end

# ------------------------------------------------------------------------------

each_project_from_csv do |project|
  message_line_base = project_detail_line_for(project)

  unless projects_has_topic?(project, NEW_TOPIC_TO_ADD)
    info("skipped, doesn't have topic - #{message_line_base}, topics=#{project['topics']}")
    next
  end

  success("removed - #{message_line_base}") if remove_topic_from_project(project)
end
