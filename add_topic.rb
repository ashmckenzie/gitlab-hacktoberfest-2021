#!/usr/bin/env /Users/ash/.asdf/shims/ruby

# frozen_string_literal: true

require_relative 'base'
require_relative 'base_topic'

$stdout.sync = true

def add_topic_to_project(project)
  new_topics = project['topics'] | [NEW_TOPIC_TO_ADD]
  debug("project: #{project_detail_line_for(project)}, current_topics=#{project['topics']}, new_topics=#{new_topics}")

  update_topics_for_project(project, new_topics)
end

# ------------------------------------------------------------------------------

each_project_from_csv do |project|
  message_line_base = project_detail_line_for(project)

  if projects_has_topic?(project, NEW_TOPIC_TO_ADD)
    info("skipped, already has topic - #{message_line_base}, topics=#{project['topics']}")
    next
  end

  success("added - #{message_line_base}") if add_topic_to_project(project)
end
