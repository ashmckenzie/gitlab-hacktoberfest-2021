default:
	@echo "INFO: Consult the README.md for instructions on how to run."

add_topic:
	@./add_topic.rb

remove_topic:
	@./remove_topic.rb
