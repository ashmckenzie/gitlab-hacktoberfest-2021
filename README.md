# GitLab Hacktoberfest 2021

This project contains two main files, `add_topic.rb` and `remove_topic.rb` which
add or remove the `Hacktoberfest` project topic, using https://gitlab.com/gitlab-data/analytics/blob/master/transform%2Fsnowflake-dbt%2Fdata%2Fprojects_part_of_product.csv
as the source of truth.

The tools here exist to support https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/general/-/issues/103.

## How to run

:warning: Ensure you have a GitLab Personal Access Token set in the `GITLAB_TOKEN` environment variable.

### To add topic

1. Run `make add_topic` to add the `Hacktoberfest` project topic.

### To remove topic

1. Run `make remove_topic` to remove the `Hacktoberfest` project topic.
