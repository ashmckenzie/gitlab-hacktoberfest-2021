# frozen_string_literal: true

require 'json'
require 'date'
require 'pathname'
require 'net/http'

BASE_REST_URL = 'https://gitlab.com/api/v4%s'

class JSONResponse
  attr_reader :response

  def initialize(response)
    @response = response
  end

  def code
    response.code
  end

  def data
    @data ||= JSON.parse(response.body)
  end
end

def error(message)
  warn "ERROR: #{message}"
end

def success(message)
  puts "SUCCESS: #{message}"
end

def info(message)
  puts "INFO: #{message}"
end

def debug(message)
  return unless debug?

  puts "DEBUG: #{message}"
end

def debug?
  ENV.fetch('DEBUG', 'false') == 'true'
end

def gitlab_token
  @gitlab_token ||= ENV['GITLAB_TOKEN'] or raise "Could not find an API token set in ENV['GITLAB_TOKEN']"
end

def get(uri, params = {}, headers = {})
  _get(uri, 1, [], params, headers).flatten
end

def _get(uri, page, result, params, headers)
  page = page.to_i
  params = params.merge({ page: page, per_page: 100 })

  json_response = request(Net::HTTP::Get, uri, params, headers)
  result << json_response

  if json_response.response['x-page'] && json_response.response['x-page'] != json_response.response['x-total-pages']
    _get(uri, json_response.response['x-next-page'], result, params, headers)
  end

  result
end

def request(klass, uri, params, headers)
  response = nil

  uri.query = URI.encode_www_form(params)
  headers.merge!(default_headers)

  Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
    request = klass.new(uri, headers)
    response = http.request(request)
  end

  JSONResponse.new(response)
end

def default_headers
  @default_headers ||= { 'Content-Type' => 'application/json', 'PRIVATE-TOKEN' => gitlab_token }
end

def uri_for_rest_path(path)
  URI.parse(format(BASE_REST_URL, path))
end

def gitlab_get(path, params = {}, headers = {})
  get(uri_for_rest_path(path), params, headers)
end

def gitlab_post(path, params = {}, headers = {})
  request(Net::HTTP::Post, uri_for_rest_path(path), params, headers)
end

def gitlab_put(path, params = {}, headers = {})
  request(Net::HTTP::Put, uri_for_rest_path(path), params, headers)
end

def get_project(id)
  gitlab_get("/projects/#{id}").first
end
